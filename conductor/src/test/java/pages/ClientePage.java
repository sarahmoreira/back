package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ClientePage extends Utils {
	
	@FindBy(xpath = "//input[@name='username']")
	private WebElement cmpLogin;
	@FindBy(xpath = "//input[@name='password']")
	private WebElement cmpSenha;
	@FindBy(xpath = "//button[@class='btn btn-primary']")
	private WebElement btnSignIn;
	@FindBy(xpath = "//*[@href='/desafioqa/incluirCliente']")
	private WebElement btnIncCliente;
	@FindBy(xpath = "//*[@href='/desafioqa/incluirVenda']")
	private WebElement btnIncVenda;
	@FindBy(xpath = "//*[@href='/desafioqa/listarCliente']")
	private WebElement btnListCliente;	
	@FindBy(xpath = "//*[@href='/desafioqa/listarVenda']")
	private WebElement btnListVenda;
	
	
	
	
	public void fazerLogin() {
		escrever(cmpLogin, "admin");
		escrever(cmpSenha, "admin");
		clicar(btnSignIn);	
	}
	
	public void inserirCliente() {
		clicar(btnIncCliente);
	}
	
	
	
	

}
