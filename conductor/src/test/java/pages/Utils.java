package pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class Utils {
	
	WebDriver driver;
	
	//Escrever
	public void escrever(WebElement elemento, String texto) {
		elemento.sendKeys(texto);	
	}
	
	public void entrarFrame() {
		WebElement iframe = driver.findElement(By.xpath("//body[@id=\"login\"]"));
		driver.switchTo().frame(iframe);
	}
	
	//cliclar
	public void clicar(WebElement elemento) {
		elemento.click();
	}
	
	
	//limpar
	
	public void limparCampo(WebElement elemento) {
		elemento.clear();
	}
	
	//avan�arbrowser
	public void avancarBrowser() {
	driver.navigate().forward();
	}
	//voltarbrowser
	public void voltarBrowser() {	
	driver.navigate().back();
	}
	
	//obterURLAtual//
	public void obterUrlAtual(String ulr) {
		driver.getCurrentUrl();
	}
	//atualizar 
	public void atualizarBrowser() {
		driver.navigate().refresh();
	}
	
	
		//obterTitulo//
	public String obterTitulo() {
		return driver.getTitle();
	}
	
	public void imprimirTitulo() {
		System.out.println(obterTitulo());
	}
	
	//abrirNovoNavegador
	
	//guardarDocumento
	
	//converter em lista
	
	//esperarSerclic�vel
	
	//esperar
	public void esperar() {
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
	}
	
	//validar mensagem
	public boolean verificarMensagem(WebElement elemento, String texto) {
		String mensagemExibida = elemento.getText();
		if (mensagemExibida.contains(texto)) {
			return true;
		}else{
			return false;
		}
	}
	
   
	
	//tirarPrint
	
	/*
	 * public class SeleniumTakeScreenshot { public static void main(String args[])
	 * throws IOException { WebDriver driver = new ChromeDriver();
	 * driver.get("http://www.example.com"); File scrFile =
	 * ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	 * FileUtils.copyFile(scrFile, new File("./image.png")); driver.quit(); } }
	 */
	
	//criarAnexo
	
	//excluirAnexo
	
	//comboBoxvalue
	public void comboBoxValor(By elemento, String atributo) {
	WebElement select = driver.findElement(elemento);
	Select combo = new Select(select);
	combo.selectByValue(atributo);
	}
	
	
	

}
