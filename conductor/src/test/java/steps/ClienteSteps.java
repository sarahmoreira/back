package steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.AbrirBrowser;
import pages.ClientePage;
import pages.Utils;

public class ClienteSteps {
	
	private static AbrirBrowser navegador = new AbrirBrowser();
	private static ClientePage cliente = new ClientePage();

	
	
	@Given("^que abra o site \"([^\"]*)\"$")
	public void que_abra_o_site(String site) throws Throwable {
		navegador.abrirNavegador(site);
	}

	@And("^informar os dados de acesso$")
	public static void fazerLogin() throws Throwable {
		cliente.fazerLogin();
	}



}
