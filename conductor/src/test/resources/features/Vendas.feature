@PrimeiroTeste 
Feature: Como us�rio devo acessar o site informando o login e senha.
Na tela home devo ter atalho para realizar a inclus�o de uma venda,
Assim como realizar a consulta da mesma.
A venda s� poder� ser realizada com sucesso caso o saldo do cliente seja igual ou superior ao valor de compra.
Caso o saldo do cliente seja inferior ao valor de compro a venda n�o dever� ser realizada.
Qualquer venda realizada dever� ser subtra�da do saldo do cliente


Background: Acessar sistema
	Given que abra o site "http://provaqa.marketpay.com.br:9082/desafioqa/listarCliente" 
	And informar os dados de acesso 

Scenario: Incluir Venda com valor inferior ao saldo do cliente
	When selecionar QA > Transa��es > Incluir
	And preecher os campo para inclus�o de uma venda
	And selecionar Salvar
	Then ser� apresentado a mensagem venda realizada com sucesso
	
Scenario: Incluir Venda com valor superior ao saldo do cliente
	Given que abra o site "http://provaqa.marketpay.com.br:9082/desafioqa/listarCliente" 
	And informar os dados de acesso 
	When selecionar QA > Transa��es > Incluir
	And preecher os campo para inclus�o de uma venda
	And selecionar Salvar
	Then ser� apresentado a mensagem valor da venda superior ao saldo 		
	
Scenario: Consultar Venda 
	Given que abra o site "http://provaqa.marketpay.com.br:9082/desafioqa/listarCliente" 
	And informar os dados de acesso 
	When selecionar QA > Cliente > Listar
	And informar o nome do Cliente que j� possua uma venda cadastrada
	And selecionar Pesquisar
	Then em tela ser� apresentado o cliente buscado
	
	
	
	
	